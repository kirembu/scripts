from easyzone import easyzone
import glob
import os
import sys

def csvzonerecords(zonefile):
    """Accepts a path to a zonefile .db and converts it to csv"""

    domain = os.path.split(zonefile)[-1][:-3]
    z = easyzone.zone_from_file(domain, zonefile)
    
    a = z.root.records('A').items if  z.root.records('A') else ['None']
    ns  = z.root.records('NS').items if z.root.records('NS') else ['None']
    mxrecords  =  z.root.records('MX').items if z.root.records('MX') else ['None']
    
    print "%s,%s,%s,%s" % (
    z.domain,
    ','.join(a) if a else ['None'],
    ','.join(ns) if ns else ['None'],
    ','.join(''.join(str(mx).strip('()')) if mxrecords is not None else ['None'] for mx in mxrecords),
    
    )


if __name__ == "__main__":
    for zonefile in os.listdir(sys.argv[1]): 
        try:
            csvzonerecords(os.path.join(sys.argv[1],zonefile))
        except Exception, e:
            print "ERROR,%s,%s" % (zonefile, e)
