mkdir -p ~/smtp/{orig,new} # Create working directories

for domain in $(cat domains.txt); do  # Backup zone files to be modified
    sudo cp -p /var/named/$domain.db ~/smtp/orig/
done


sudo cp ~/smtp/orig/*db ~/smtp/new/ # New zone files

sudo chown pmithamo: ~/smtp/new/*db # Chown now zone files for working 

perl -pi -e "s/smtp.liquidtelecom.co.ke.$/smtp.swiftkenya.com./g" ~/smtp/new/*db # Replace smtp.liquid

grep Serial ~/smtp/new/*db | awk '{print $2}' | sort | uniq # Find serials in files

perl -pi -e "s/(2015031302|2015031303|2015031305)/2015031700/g" ~/smtp/new/*db # Update serials in file

for file in *db; do echo sudo /scripts/dnscluster synczone ${file%.*} ; done | sh # Run synzone script

