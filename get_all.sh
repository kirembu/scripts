# Get domain from CSV dump
# Get first filed in CSV dump delimited with comma
## cut -d, -f 1 fetchcsv 

# Get NS from registry of domain
for domain in $(cat domains.txt); do
    echo $domain","$(dig ${domain} a +short)"," $(dig @$(dig ${domain##*.} ns +short | head -1) $domain ns | egrep "\sIN\s+NS\s" | awk '{print $5}' | tr "\\n" ",") $(dig ${domain} mx +short)
done
