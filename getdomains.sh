# Get domain from CSV dump
# Get first filed in CSV dump delimited with comma
## cut -d, -f 1 fetchcsv 

# Get NS from registry of domain
for domain in $(cat all_domains_final_gtld.txt); do
echo $domain $(dig @$(dig ${domain##*.} ns +short | head -1) $domain ns | egrep "\sIN\s+NS\s" | awk '{print $5}' | tr "\\n" ",")
done
