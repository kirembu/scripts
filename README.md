# Scripts#

A collection of scripts I use on a day to day basis... Mostly in python.

### What is this repository for? ###

Any script that makes work and life easier is welcome.

### How do I get set up? ###

Install Python if you are on Windows.  
Install PIP  
..  
..  
$ pip -r requirements.txt  
..  
..  
Profit

### Contribution guidelines ###

Python is preferred though Bash and other languages are more than welcome.

### Who do I talk to? ###

@kirembu